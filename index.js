// Import thư viện expressjs tương đương import express from "express";
const express = require("express");
// Import thư viện path
const path = require("path");
// Khởi tạo 1 app express
const app = express();
// khai báo cổng chạy project
const port = 8000;
//Callback function là 1 function đóng vài trò là tham số của 1 function khác, nó sẽ thực hiện khi function chủ được gọi
// Khai báo API dạng /
app.get("/", (req, res) => {
    let currentDay = new Date();
    /*res.json({
        message: `Xin chào các bạn, hôm nay là ngày ${currentDay.getDate()} tháng ${currentDay.getMonth() + 1} năm ${currentDay.getFullYear()}`
    })
    */
    //res.send("<h1>Hello World</h1>");
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/index.html"));
});

app.get("/about", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/about.html"));
});

app.get("/sitemap", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/sitemap.html"));
});
// khai báo để sử dụng tài nguyên tĩnh (images, css, js...)
app.use(express.static(__dirname + "/views"))
app.listen(port, () => {
    console.log("App listening on port: ", port);
})